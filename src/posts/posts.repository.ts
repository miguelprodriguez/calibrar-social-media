import { BadRequestException, ConsoleLogger } from "@nestjs/common";
import { AuthEntity } from "src/auth/auth.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreatePostDto } from "./dto/create-post.dto";
import { searchPostDto } from "./dto/search-post.dto";
import { PostsEntity } from "./posts.entity";

@EntityRepository(PostsEntity)
export class PostsRepository extends Repository<PostsEntity> {

    createPost = async (
        createPostDto: CreatePostDto,
        auth: AuthEntity
    ): Promise<PostsEntity> => {

        const { profile } = auth
        if (profile === null) {
            throw new BadRequestException({
                message: "Please create an account first"
            })
        }
        const post = await this.create({ ...createPostDto, profile })
        await this.save(post)
        return post
    }

    // getAllPosts = async (): Promise<PostsEntity[]> => { 
    getAllPosts = async (
        searchedByUser: searchPostDto
    ): Promise<PostsEntity[]> => {

        // https://github.com/typeorm/typeorm/blob/master/docs/select-query-builder.md#what-is-querybuilder
        const query = await this.createQueryBuilder('posts')
        
        const values = Object.values(searchedByUser)

        if (values.length > 0) {
            // SQL like syntax
            query.where('LOWER(posts.body) LIKE LOWER(:search)', { search: `%${values}%` })
        }

        const posts = await query.getMany()
        return posts
    }

}