import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthEntity } from 'src/auth/auth.entity';
import { GetAuth } from 'src/auth/decorator/get-auth.decorator';
import { CreatePostDto } from './dto/create-post.dto';
import { searchPostDto } from './dto/search-post.dto';
import { PostsEntity } from './posts.entity';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {

    constructor(private postsService: PostsService) { }

    @Post()
    @UseGuards(AuthGuard())
    createPost(
        @Body() createPostDto: CreatePostDto,
        @GetAuth() auth: AuthEntity
    ): Promise<PostsEntity> {
        return this.postsService.createPost(createPostDto, auth)
    }

    @Get()
    getAllPosts(
        @Query() searchedByUser: searchPostDto
    ): Promise<PostsEntity[]> {
        return this.postsService.getAllPosts(searchedByUser)
    }

    // http://localhost:3000/posts/[id]
    @Get('/:id')
    getPostById(
        @Param('id') id: string
    ): Promise<PostsEntity> {
        return this.postsService.getPostById(id)
    }

    @Patch('/:id')
    @UseGuards(AuthGuard())
    updatePostById(
        @Param('id') id: string,
        @Body() newUpdate: CreatePostDto,
        @GetAuth() auth: AuthEntity
    ): Promise<PostsEntity> {
        return this.postsService.updatePostById(id, newUpdate, auth)
    }

    @Delete('/:id')
    @UseGuards(AuthGuard())
    deletePostById(
        @Param('id') id: string,
        @GetAuth() auth: AuthEntity
    ): Promise<void> {
        return this.postsService.deletePostById(id, auth)
    }

}
