import { IsOptional } from "class-validator";

export class searchPostDto {
    @IsOptional()
    search: string
}