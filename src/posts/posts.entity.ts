import { ProfileEntity } from "src/profile/profile.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class PostsEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    body: String

    // https://github.com/typeorm/typeorm/issues/4838
    @CreateDateColumn({ type: "timestamp", onUpdate: 'CURRENT_TIMESTAMP', nullable: true })
    posted_date: Date

    @ManyToOne(() => ProfileEntity, profile => profile.posts, { eager: false })
    profile: ProfileEntity
}