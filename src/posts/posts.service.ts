import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthEntity } from 'src/auth/auth.entity';
import { CreatePostDto } from './dto/create-post.dto';
import { searchPostDto } from './dto/search-post.dto';
import { PostsEntity } from './posts.entity';
import { PostsRepository } from './posts.repository';

@Injectable()
export class PostsService {

    constructor(
        @InjectRepository(PostsRepository)
        private postsRepository: PostsRepository
    ) { }

    createPost = async (
        createPostDto: CreatePostDto,
        auth: AuthEntity
    ): Promise<PostsEntity> => {
        return this.postsRepository.createPost(createPostDto, auth)
    }

    getAllPosts = (searchedByUser: searchPostDto): Promise<PostsEntity[]> => {
        return this.postsRepository.getAllPosts(searchedByUser)
    }

    getPostById = async (id: string): Promise<PostsEntity> => {
        const isPostFound = await this.postsRepository.findOne(id)

        if (!isPostFound) {
            throw new NotFoundException({
                message: `${id} not found`
            })
        }
        return isPostFound
    }

    updatePostById = async (id, newUpdate, auth): Promise<PostsEntity> => {
        const { profile } = auth

        const postToUpdate = await this.postsRepository.findOne({ id, profile })
        postToUpdate.body = newUpdate.body

        try {
            await this.postsRepository.save(postToUpdate)
            return postToUpdate
        } catch (error) {
            throw new InternalServerErrorException()
        }

    }

    deletePostById = async (id, auth): Promise<void> => {
        const { profile } = auth
        const deleteResult = await this.postsRepository.delete({ id, profile })
        if (deleteResult.affected === 0) {
            throw new NotFoundException()
        }
    }

}
