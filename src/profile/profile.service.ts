import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { profile } from 'console';
import { async } from 'rxjs';
import { AuthEntity } from 'src/auth/auth.entity';
import { ProfileFillDetailsDto } from './dto/profile-fill-details.dto';
import { ProfileUpdateDto } from './dto/profile-update-name.dto';
import { ProfileEntity } from './profile.entity';
import { ProfileRepository } from './profile.repository';

@Injectable()
export class ProfileService {
    constructor(
        @InjectRepository(ProfileRepository)
        private profileRepository: ProfileRepository
    ) { }

    fillDetails = async (
        profileFillDetailsDto: ProfileFillDetailsDto,
        auth: AuthEntity
    ): Promise<ProfileEntity> => {
        return this.profileRepository.fillDetails(profileFillDetailsDto, auth)
    }

    getProfileById = async (id: string): Promise<ProfileEntity> => {
        const profileFound = await this.profileRepository.findOne(id)
        const auth = await this.profileRepository.find({ relations: ["auth"] })
        if (!profileFound) {
            throw new NotFoundException({
                message: "User not found"
            })
        }

        return profileFound
    }

    updateProfileById = async (
        id: string,
        auth: AuthEntity,
        newDetails: ProfileUpdateDto
    ): Promise<ProfileEntity> => {
        const profileToUpdate = await this.profileRepository.findOne({ id, auth })

        if (!profileToUpdate) {
            throw new NotFoundException("Your profile cannot be found.")
        }

        const updated = Object.assign(profileToUpdate, newDetails)

        await this.profileRepository.save(updated)
        return profileToUpdate
    }
}
