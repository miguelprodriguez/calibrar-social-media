import { ConflictException } from "@nestjs/common";
import { async } from "rxjs";
import { AuthEntity } from "src/auth/auth.entity";
import { EntityRepository, Repository } from "typeorm";
import { ProfileFillDetailsDto } from "./dto/profile-fill-details.dto";
import { GenderEnum } from "./enum/profile-gender.enum";
import { ProfileEntity } from "./profile.entity";

@EntityRepository(ProfileEntity)
export class ProfileRepository extends Repository<ProfileEntity> {

    fillDetails = async (
        profileFillDetailsDto: ProfileFillDetailsDto,
        auth: AuthEntity
    ): Promise<ProfileEntity> => {

        const profile = this.create({ ...profileFillDetailsDto, auth })

        try {
            await this.save(profile)
            return profile
        } catch (error) {
            if(error.code === '23505') {
                throw new ConflictException('You have already created your profile')
            }
        }

    }
}