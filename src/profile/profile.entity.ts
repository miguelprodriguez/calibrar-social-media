import { AuthEntity } from "src/auth/auth.entity";
import { PostsEntity } from "src/posts/posts.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { GenderEnum } from "./enum/profile-gender.enum";

@Entity()
export class ProfileEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    full_name: String

    @Column()
    email: String

    @Column()
    gender: GenderEnum

    @Column()
    photo: String

    // https://github.com/typeorm/typeorm/issues/4838
    @CreateDateColumn({ type: "timestamp", nullable: true })
    created_at: Date

    @OneToOne(() => AuthEntity, { onDelete: "CASCADE" })
    @JoinColumn()
    auth: AuthEntity

    // https://typeorm.io/#/eager-and-lazy-relations
    @OneToMany(() => PostsEntity, post => post.profile, { eager: true })
    posts: PostsEntity[]
}