import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthEntity } from 'src/auth/auth.entity';
import { GetAuth } from 'src/auth/decorator/get-auth.decorator';
import { ProfileFillDetailsDto } from './dto/profile-fill-details.dto';
import { ProfileUpdateDto } from './dto/profile-update-name.dto';
import { ProfileEntity } from './profile.entity';
import { ProfileService } from './profile.service';

@Controller('profile')
@UseGuards(AuthGuard())
export class ProfileController {
    constructor(private profileService: ProfileService) { }

    @Post()
    ProfileFillDetailsDto(
        @Body() profileFillDetailsDto: ProfileFillDetailsDto,
        @GetAuth() auth: AuthEntity
    ): Promise<ProfileEntity> {
        return this.profileService.fillDetails(profileFillDetailsDto, auth)
    }

    @Get('/:id')
    getProfileById(
        @Param('id') id: string,
        @GetAuth() auth: AuthEntity
    ): Promise<ProfileEntity> {
        return this.profileService.getProfileById(id)
    }

    @Patch('/:id')
    updateProfileById(
        @Param('id') id: string,
        @GetAuth() auth: AuthEntity,
        @Body() newDetails: ProfileUpdateDto
    ): Promise<ProfileEntity> {
        return this.profileService.updateProfileById(id, auth, newDetails)
    }
}

