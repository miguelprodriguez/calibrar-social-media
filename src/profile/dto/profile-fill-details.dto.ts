import { IsString, MinLength, MaxLength, IsEmail, IsEnum } from "class-validator"
import { GenderEnum } from "../enum/profile-gender.enum"

export class ProfileFillDetailsDto {

    @IsString()
    @MinLength(2)
    @MaxLength(20)
    full_name: String

    @IsEmail()
    email: String

    @IsEnum(GenderEnum, {
        message: 'You must select MALE or FEMALE'
    })
    gender: GenderEnum

    @IsString()
    photo: String

}