import { IsString, MinLength, MaxLength, IsOptional, IsEmail, IsEnum } from "class-validator"
import { GenderEnum } from "../enum/profile-gender.enum"

export class ProfileUpdateDto {

    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(20)
    full_name: String

    @IsOptional()
    @IsEmail()
    email: String

    @IsOptional()
    @IsEnum(GenderEnum, {
        message: 'You must select MALE or FEMALE'
    })
    gender: GenderEnum

    @IsOptional()
    @IsString()
    photo: String
}