import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { AuthRepository } from "../auth.repository";
import { ExtractJwt, Strategy } from "passport-jwt"
import { AuthEntity } from "../auth.entity";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(
        @InjectRepository(AuthRepository)
        private authRepository: AuthRepository
    ) {
        super({
            secretOrKey: 'hard!to-guess_secret',
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
        })
    }

    validate = async (payload: { username: String }): Promise<AuthEntity> => {
        const { username } = payload
        const user: AuthEntity = await this.authRepository.findOne({ username })

        if (!user) {
            throw new UnauthorizedException()
        }

        return user
    }
}