import { Injectable, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { AuthRepository } from "./auth.repository";
import { AuthRegistrationDto } from "./dto/auth-registration.dto";
import { AuthLoginDto } from "./dto/auth-login.dto";
import * as bcrypt from 'bcrypt'
import { AuthEntity } from "./auth.entity";
import { ProfileEntity } from "src/profile/profile.entity";

@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(AuthRepository)
        private authRepository: AuthRepository,
        private jwtService: JwtService
    ) { }

    async createAccount(authCredentialsDto: AuthRegistrationDto): Promise<{ message: String }> {
        return await this.authRepository.createAccount(authCredentialsDto)
    }

    signIn = async (authLoginDto: AuthLoginDto): Promise<{ accessToken: String }> => {
        const { username, password } = authLoginDto

        const isUserFound = await this.authRepository.findOne({ username })
        const isPasswordCorrect = isUserFound && await bcrypt.compare(password, isUserFound.password)

        if (isUserFound && isPasswordCorrect) {
            const payload = { username }
            const accessToken = await this.jwtService.sign(payload)
            return { accessToken }
        } else {
            throw new UnauthorizedException('Please check your login credentials')
        }
    }

    deleteAccount = async (id: String, auth: AuthEntity): Promise<void> => {
        const { profile } = auth
        console.log("id: ", id)
        console.log("Profile: ", profile)
        const accountToDelete = await this.authRepository.findOne({ id, profile })
        console.log(accountToDelete)
        if (!accountToDelete) {
            throw new UnauthorizedException('Please check your credentials')
        }
        await this.authRepository.delete(accountToDelete)
    }

}