import { ConflictException } from "@nestjs/common"
import { EntityRepository, Repository } from "typeorm"
import { AuthEntity } from "./auth.entity"
import { AuthRegistrationDto } from "./dto/auth-registration.dto"
import * as bcrypt from 'bcrypt'

@EntityRepository(AuthEntity)
export class AuthRepository extends Repository<AuthEntity> {
    
    createAccount = async (authRegistrationDto: AuthRegistrationDto): Promise<{ message: string }> => {
        const { username, password } = authRegistrationDto

        const saltOrRounds = 10;
        const hashedPassword = await bcrypt.hash(password, saltOrRounds)

        const auth = this.create({
            username,
            password: hashedPassword
        })

        try {
            await this.save(auth)
            return { message: "You have successfully registered" }
        } catch (error) {
            if (error.code === "23505") {
                throw new ConflictException("Username already exists")
            } 
            // When an exception is unrecognized 
            // /neither HttpException nor a class that inherits from HttpException
            // the built-in exception filter generates the following 
            // default JSON response: 
            // { "statusCode": 500, "message": "Internal server error"}
        }
    }

}