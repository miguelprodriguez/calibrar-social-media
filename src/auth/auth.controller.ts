import { Body, Controller, Delete, Post, Param, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProfileEntity } from 'src/profile/profile.entity';
import { AuthEntity } from './auth.entity';
import { AuthService } from './auth.service';
import { GetAuth } from './decorator/get-auth.decorator';
import { AuthLoginDto } from './dto/auth-login.dto';
import { AuthRegistrationDto } from './dto/auth-registration.dto';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) { }

    @Post('register')
    createAccount(
        @Body() authRegistrationDto: AuthRegistrationDto
    ): Promise<{ message: String }> {
        return this.authService.createAccount(authRegistrationDto)
    }

    @Post('login')
    signIn(
        @Body() authLoginDto: AuthLoginDto
    ): Promise<{ accessToken: String }> {
        return this.authService.signIn(authLoginDto)
    }

    @UseGuards(AuthGuard())
    @Delete('/:id')
    deleteAccount(
        @Param('id') id: String,
        @GetAuth() auth: AuthEntity
    ): Promise<void> {
        return this.authService.deleteAccount(id, auth)
    }

}
