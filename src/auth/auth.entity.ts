import { ProfileEntity } from 'src/profile/profile.entity';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class AuthEntity {
  @PrimaryGeneratedColumn('uuid')
  id: String;

  @Column({ unique: true })
  username: String;

  @Column()
  password: String;

  @OneToOne(() => ProfileEntity, profile => profile.auth, { eager: true, onDelete: "CASCADE"})
  profile: ProfileEntity

}
